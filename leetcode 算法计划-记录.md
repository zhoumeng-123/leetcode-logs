## leetcode 记录

## 3.3

#### [704. 二分查找](https://leetcode-cn.com/problems/binary-search/)

有序 二分

```c++
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int len=nums.size();
        if(len==0) return -1;
        if (target < nums[0] || target > nums[len - 1]) 
            return -1;
        // if(len==1&&nums[0]==target) return 0;
        int left=0, right=len-1, mid=(left+right)/2;
        while(left<=mid)   //  <=  when len==1
        {
            if(nums[mid]==target) return mid;
            else if(nums[mid]<target)
            {
                left=mid+1;    //+1
                // mid=(mid+right)/2;  nope 
            }
            else
            {
                right=mid-1;
                // mid=(mid+left)/2;
            }
            mid=(left+right)/2;
        }
        return -1;
    }
};
```

1. 考虑len==0 ==1(left<=mid情况)
2. 循环中应  mid=(left+right)/2; 否则mid=(mid+left)/2当left==mid将出错
3. left=mid+1   应+1
4. 先考虑 if (target < nums[0] || target > nums[len - 1])避免循环

#### [278. 第一个错误的版本](https://leetcode-cn.com/problems/first-bad-version/)

   1. 不要用（left+right）/ 2 形式，应该用 left+(right-left)/2 来防止求中值时候的溢出

      ```c++
      mid=left+((right-left)>>1);  //left+
      ```

   2. right=mid; // 有可能mid就是第一个bad，不能-1

#### [35. 搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)

```c++
	    if(len==0) return 0;
        int left=0, right=len-1, mid;
        while(left<=right)   
        {
            mid=left+((right-left)>>1);
            if(nums[mid]==target) return mid;
            else if(nums[mid]<target)
                left=mid+1;    
            else
                right=mid-1;
        }
        return left;
```

## 3.4

#### [977. 有序数组的平方](https://leetcode-cn.com/problems/squares-of-a-sorted-array/)

1. vector<int> ans(n,0)     

   vector<vector<int>> ans(3,vector<int>(4,1))  // 3*4

   二维 ans[i].resize(n)   ans.resize(n,v0)

#### 		[vector的初始化](https://blog.csdn.net/qq_40147449/article/details/87892312 )

2. 传入引用&，faster~    fun(vector<int>& nums)

3. ```c++
   sort(ans.begin(), ans.end()); //O(nlogn)
   ```

4. 双指针 从中间向两边 或从两边向中间（逆序放入ans），无需分别考虑边界

      

#### [189. 轮转数组](https://leetcode-cn.com/problems/rotate-array/)

1. int flag=len-(k%len); 向右轮转 `k` 个位置，k应对长度取余

2. 更多方法：

   方法一：使用额外的数组

   将原数组下标为 iii 的元素放至新数组下标为 (i+k) mod n 的位置，最后将新数组拷贝至原数组即可   newArr[(i + k) % n] = nums[i];

   方法二：环状替换   k = k % n; int count = gcd(k, n); 最大公约数   空间O(1)![image.png](https://pic.leetcode-cn.com/f0493a97cdb7bc46b37306ca14e555451496f9f9c21effcad8517a81a26f30d6-image.png)

   方法三：数组翻转

   先将所有元素翻转，这样尾部的 k mod nk\bmod nkmodn 个元素就被移至数组头部，然后我们再翻转 [0, k mod n −1] 区间的元素和 [k mod n,n−1] 区间的元素即能得到最后的答案

   ```c++
   void reverse(vector<int>& nums, int start, int end) {
           while (start < end) {
               swap(nums[start], nums[end]);
               start += 1;
               end -= 1;
           }
       }
   
       void rotate(vector<int>& nums, int k) {
           k %= nums.size();
           reverse(nums, 0, nums.size() - 1);
           reverse(nums, 0, k - 1);
           reverse(nums, k, nums.size() - 1);
       }
   
   ```

vector赋值——copy(ans.begin(),ans.end(),nums.begin());

注意数据大小 必要时 unsigned long long

Eternity   Crystal   tackle **substantially** **asymptotically**

## 3.5



## 3.9

#### [19. 删除链表的倒数第 N 个结点](https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/)

双指针：swap(s[i],s[len-1-i]) 两边

快慢指针 slow fast两步   or  fast先走n步etc.

本题——先遍历，或快慢指针，或递归 逆序记录倒数第n个





