## leetcode 记录——剑指offer(2)

## 9.28

#### [剑指 Offer 07. 重建二叉树](https://leetcode.cn/problems/zhong-jian-er-cha-shu-lcof/)

1. 递归：在中序遍历中定位到根节点，可以分别知道左子树和右子树中的节点数目。(由于同一颗子树的前序遍历和中序遍历的长度显然是相同的，因此）可以对应到前序遍历的结果中，对上左右子树——左右括号进行定位。

   递归地对构造出左子树和右子树，再将这两颗子树接到根节点的左右位置。

```c++
class Solution {
private:
    unordered_map<int, int> index;  //map 红黑树 hansh_map or unordered_map 是哈希表 （函数外定义 或者传指针）

public:
    TreeNode* myBuildTree(const vector<int>& preorder, const vector<int>& inorder, int preorder_left, int preorder_right, int inorder_left, int inorder_right) {
        if (preorder_left > preorder_right) {
            return nullptr;
        }
        
        // 前序遍历中的第一个节点就是根节点
        int preorder_root = preorder_left;
        // 在中序遍历中定位根节点
        int inorder_root = index[preorder[preorder_root]];
        
        // 先把根节点建立出来
        TreeNode* root = new TreeNode(preorder[preorder_root]);
        // 得到左子树中的节点数目
        int size_left_subtree = inorder_root - inorder_left;
        // 递归地构造左子树，并连接到根节点
        // 先序遍历中「从 左边界+1 开始的 size_left_subtree」个元素就对应了中序遍历中「从 左边界 开始到 根节点定位-1」的元素
        root->left = myBuildTree(preorder, inorder, preorder_left + 1, preorder_left + size_left_subtree, inorder_left, inorder_root - 1);
        // 递归地构造右子树，并连接到根节点
        // 先序遍历中「从 左边界+1+左子树节点数目 开始到 右边界」的元素就对应了中序遍历中「从 根节点定位+1 到 右边界」的元素
        root->right = myBuildTree(preorder, inorder, preorder_left + size_left_subtree + 1, preorder_right, inorder_root + 1, inorder_right);
        return root;
    }

    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        int n = preorder.size();
        // 构造哈希映射，帮助我们快速定位根节点
        for (int i = 0; i < n; ++i) {
            index[inorder[i]] = i;
        }
        return myBuildTree(preorder, inorder, 0, n - 1, 0, n - 1);
    }
};

```

2. 迭代：对于前序遍历中的任意两个连续节点 u 和 v，根据前序遍历的流程，我们可以知道 u 和 v 只有两种可能的关系：

    v 是 u 的左儿子。这是因为在遍历到 u之后，下一个遍历的节点就是 u的左儿子；
    
    u没有左儿子，并且 v是 u的某个祖先节点（或者 u本身）的右儿子。

```c++
class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        if(!preorder.size()){   //判断是否为空
            return nullptr;
        }
        TreeNode* root = new TreeNode(preorder[0]); //先序的首为根节点
        stack<TreeNode*> stk;   //建立栈
        stk.push(root); //根节点入栈
        int inorderIndex = 0;   //扫描中序的指针
        for(int i = 1; i < preorder.size(); i++){   //从先序遍历开始逐个遍历
            TreeNode *node = stk.top();
            if(node->val != inorder[inorderIndex]){ //栈顶元素的值与中序遍历当前所指的元素值不等
                node->left = new TreeNode(preorder[i]); //前序遍历中处在栈顶元素位置后一位的元素是栈顶元素的左子树
                stk.push(node->left);   //栈顶元素左子树节点入栈
            }else{  //栈顶元素的值与中序遍历当前所指的元素值相等,栈顶即为最左下角的树节点
                while(!stk.empty() && stk.top()->val == inorder[inorderIndex]){ //while循环向上返回，寻找位置进行右子树的重建
                    node = stk.top();   //指针向右扫描中序遍历
                    stk.pop();  //栈中所有与当前指针所指元素值相等的节点出栈
                    inorderIndex++;
                }
                node->right = new TreeNode(preorder[i]);    // 循环结束后，node所指栈顶元素即是需要重建右子树的节点
                stk.push(node->right);
            }
        }
        return root;    
    }
};
```

#### [剑指 Offer 09. 用两个栈实现队列](https://leetcode.cn/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof/)

双栈

```c++
class CQueue {
    private:
    stack<int> inStack, outStack;
public:
    CQueue() { }
    
    void appendTail(int value) 
    {
        inStack.push(value);
    }
    
    int deleteHead() 
    {
        if(outStack.empty())
        {
            if(inStack.empty())
                return -1;
            while(!inStack.empty())
            {
                outStack.push(inStack.top());
                inStack.pop();
            }
        }
        int res = outStack.top();
        outStack.pop();
        return res;
    }
};
```

9.29











